const request = require('request-promise-native');
const fs = require('fs');

const requestPromises = fs
  .readFileSync('index_en_sbtl_minnaram.m3u8')
  .toString()
  .split('\n')
  .filter((line) => {
    return (!line.startsWith('#') && line.length);
  })
  .map((uri, index) =>{
    return request({
      method:'GET',
      uri
    });
  });

Promise.all(requestPromises)
  .then(responses =>{
    fs.writeFile('minnaram_en_sbtl.webvtt', responses.join(''), function(err) {
      if(err) {
        return console.log(err);
      }
      console.log("The subtitle file was saved!");
    });
  });

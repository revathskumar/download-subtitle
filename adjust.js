const fs = require('fs');
const moment = require('moment');

const ADJUST_TIME = '01:12:04';

const lines = fs
  .readFileSync('cd2.srt')
  .toString()
  .split('\n')
  .map(line => {
    const [match, startTime, splitter, endTime, ender ] = line.match(/(^\d{2}:\d{2}:\d{2})(.*)(\d{2}:\d{2}:\d{2})(.*)/) || [];
    if (match) {
      console.log('start Time :: ', startTime)
      console.log('End Time :: ', endTime)
      const adjustedStartTime = moment(startTime, 'HH:mm:ss').add(moment.duration(ADJUST_TIME)).format('HH:mm:ss');
      const adjustedEndTime = moment(endTime, 'HH:mm:ss').add(moment.duration(ADJUST_TIME)).format('HH:mm:ss');
      const adjustedLine = `${adjustedStartTime}${splitter}${adjustedEndTime}${ender}`;
      console.log('new adjusted line ', adjustedLine);
      return adjustedLine;
    }
    return line;
  });

  fs.writeFile('cd2-adjusted.srt', lines.join('\n'), function(err) {
    if(err) {
      return console.log(err);
    }
    console.log("The subtitle adjusted file was saved!");
  });